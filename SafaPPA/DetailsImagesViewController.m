//
//  DetailsImagesViewController.m
//  SafaPPA
//
//  Created by Rawan Marzouq on 8/16/16.
//  Copyright © 2016 Rawan. All rights reserved.
//

#import "DetailsImagesViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MediaCollectionViewCell.h"
#import "LargeImageViewController.h"


@interface DetailsImagesViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
{
    NSMutableArray *categories;
    int catIndex;
    NSMutableArray *posts;
    
}


@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loading;

// Main post elements
@property (weak, nonatomic) IBOutlet UIImageView *mainPostImage;
@property (weak, nonatomic) IBOutlet UILabel *mainPostTitle;
@property (weak, nonatomic) IBOutlet UILabel *mainPostDate;

@property (weak, nonatomic) IBOutlet UITextView *mainPostText;
@property (nonatomic, strong) IBOutlet UICollectionView *collectionView;

@end

@implementation DetailsImagesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    // Set main post details
    NSAttributedString *attributedString = [[NSAttributedString alloc]
                                            initWithData: [self.post.postTitle dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                            documentAttributes: nil
                                            error: nil
                                            ];
    self.mainPostTitle.attributedText = attributedString;
    self.mainPostTitle.textAlignment = NSTextAlignmentRight;
    self.mainPostTitle.font = [UIFont fontWithName:@"beIN-ArabicNormal" size:17];
    self.mainPostTitle.textColor = [UIColor whiteColor];
    self.mainPostDate.text = [self FormatDateToString:self.post.postDate];
    self.mainPostDate.font = [UIFont fontWithName:@"beIN-ArabicNormal" size:13];
    self.mainPostImage.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://safa.ps/%@", self.post.postImage]]]];
    
    
    // Hide navigation bar
    self.navigationController.navigationBar.hidden = YES;
    
    // Collection view custom cell
    UINib *cellNib = [UINib nibWithNibName:@"MediaCell" bundle:nil];
    [self.collectionView registerNib:cellNib forCellWithReuseIdentifier:@"cell"];
    [self.collectionView registerClass:[MediaCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    
    // Get Images
    [self GetMediaPosts];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)Back:(id)sender {
    NSLog(@"Back");
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - REST Category's posts
-(void)GetMediaPosts{
    // Start loding
    self.loading.hidden = NO;
    
    
    posts = [[NSMutableArray alloc]init];
    
    // REST CALL
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://safa.ps/api/1.0/gallery/%i",self.post.postId]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate:nil delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Handle response
        if (data.length > 0 && error == nil)
        {
            NSError *error = nil;
            id jsonObjects = [NSJSONSerialization JSONObjectWithData:data
                                                             options:NSJSONReadingMutableContainers
                                                               error:&error];
            
            
            NSLog(@"Images: %@",jsonObjects);
            
            if (error) {
                NSLog(@"error is %@", [error localizedDescription]);
                // Handle Error and return
                return;
            }
            
            // Posts
            NSArray *postsData = [jsonObjects objectForKey:@"images"];
            for (int i=0; i < [postsData count]; i++) {
                NSDictionary *post = (NSDictionary*) [postsData objectAtIndex:i];
                Post *postObj = [[Post alloc]init];
                postObj.postId = [[post objectForKey:@"id"] intValue];
                postObj.postLink = [post objectForKey:@"link"];
                
                [posts addObject:postObj];
            }
            
            
            // Reload posts table content
            [_collectionView reloadData];
            
            // Stop loding
            self.loading.hidden = YES;
        }
        else
        {
            // Handle Error
            UIAlertController *errorVC = [UIAlertController alertControllerWithTitle:@"Network Error" message:@"Please check your connection and try again" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            }];
            [errorVC addAction:okAction];
            [self presentViewController:errorVC animated:YES completion:nil];
            NSLog(@"Error: %@",error);
        }
    }];
    [dataTask resume];
}

#pragma mark - Format Date
-(NSString*)FormatDateToString:(NSString*)epochDate{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *postDate = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:[epochDate integerValue]]];
    return postDate;
}

#pragma mark - Collection view Delegate & DataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [posts count];
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    // Setup cell identifier
    static NSString *cellIdentifier = @"cell";
    
    MediaCollectionViewCell *cell = (MediaCollectionViewCell *)[self.collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
    // Check if patients array is not null
    if ([posts count]) {
        Post *post = [posts objectAtIndex:indexPath.row];
        cell.titleLabel.text = @"";
        // Here we use the new provided sd_setImageWithURL: method to load the web image
        [cell.mediaImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://safa.ps/%@", post.postLink]] placeholderImage:[UIImage imageNamed:@"placeholder"] options:indexPath.row == 0 ? SDWebImageRefreshCached:0];
        NSLog(@"http://safa.ps/%@", post.postLink);
    }
    
    // Cell view configurations
    cell.backgroundColor = [UIColor clearColor];
    
    // Return the cell
    return cell;
    
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    Post *selPost = [posts objectAtIndex:indexPath.row];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LargeImageViewController *mediaVC = [sb instantiateViewControllerWithIdentifier:@"Image"];
    mediaVC.imageLink = selPost.postLink;
    [self presentViewController:mediaVC animated:YES completion:nil];
    
}

#pragma mark Collection view layout
// Layout: Set cell size
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //    NSLog(@"SETTING SIZE FOR ITEM AT INDEX %d", indexPath.row);
    CGSize mElementSize = CGSizeMake(self.collectionView.frame.size.height - 10, self.collectionView.frame.size.height - 10);
    return mElementSize;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 2.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 2.0;
}

// Layout: Set Edges
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5,5,5,5);  // top, left, bottom, right
    //    return UIEdgeInsetsMake(0,0,0,0);  // top, left, bottom, right
}

@end
