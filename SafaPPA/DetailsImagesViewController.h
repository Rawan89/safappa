//
//  DetailsImagesViewController.h
//  SafaPPA
//
//  Created by Rawan Marzouq on 8/16/16.
//  Copyright © 2016 Rawan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post.h"


@interface DetailsImagesViewController : UIViewController
@property(strong,nonatomic) Post *post;

@end
