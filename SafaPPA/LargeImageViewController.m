//
//  LargeImageViewController.m
//  SafaPPA
//
//  Created by Rawan Marzouq on 8/16/16.
//  Copyright © 2016 Rawan. All rights reserved.
//

#import "LargeImageViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface LargeImageViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *image;
- (IBAction)Back:(id)sender;

@end

@implementation LargeImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.image sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://safa.ps/%@", self.imageLink]] placeholderImage:[UIImage imageNamed:@"placeholder"] options:SDWebImageRefreshCached];
    NSLog(@"http://safa.ps/%@", self.imageLink);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)Back:(id)sender {
    NSLog(@"Back");
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
