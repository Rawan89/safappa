//
//  LargeImageViewController.h
//  SafaPPA
//
//  Created by Rawan Marzouq on 8/16/16.
//  Copyright © 2016 Rawan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LargeImageViewController : UIViewController
@property(strong,nonatomic) NSString *imageLink;
@end
